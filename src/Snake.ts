import {Canvas} from './Canvas.js';
import {Item} from './Item.js';
import {Position} from './Position.js';
export class Snake{
  private body:Position[];
  private xStep:number;
  private yStep:number;
  private canvas:Canvas;
  private bodylength:number;
  private bodyGrow:number;
  public constructor(x:number,y:number,c:Canvas){
    this.body=new Array();
    this.body.push(new Position(x*20,y*20));
    this.canvas=c;
    this.xStep=20;
    this.yStep=20;
    this.right();
    this.bodyGrow=5;
  }
  public step():void{
    let position:Position=new Position(this.getHeadX(),this.getHeadY());
    position.addX(this.xStep);
    position.addY(this.yStep);
    if(this.bodyGrow>0){
      this.bodyGrow--;
    }else{
      this.body.shift();
    }
    this.body.push(position);
  }
  private getHeadX():number{
    return this.body[this.body.length-1].getX();
  }
  private getHeadY():number{
    return this.body[this.body.length-1].getY();
  }
  public draw(){
    for(let i=0;i<this.body.length;i++){
      this.canvas.draw(this.body[i].getX(),this.body[i].getY(),20,20,'yellow');
    }
  }
  public up():void{
    this.yStep=-20;
    this.xStep=0;
  }
  public down():void{
    this.yStep=20;
    this.xStep=0;
  }
  public right():void{
    this.xStep=20;
    this.yStep=0;
  }
  public left():void{
    this.xStep=-20;
    this.yStep=0;
  }
  public isDead(left:number,right:number,top:number,down:number):boolean{
    if(this.checkBody()||this.getHeadX()>right*20||this.getHeadX()<left*20||this.getHeadY()<top*20||this.getHeadY()>down*20){
      return true;
    }
    return false;
  }
  private checkBody():boolean{
    for(let i:number=0;i<this.body.length-1;i++){
      if(this.body[i].getX()==this.getHeadX()&&this.body[i].getY()==this.getHeadY()){
        return true;
      }
    }
    return false;
  }
  public checkItem(item:Item):boolean{
    if(this.getHeadX()==item.getX()&&this.getHeadY()==item.getY()){
      this.bodyGrow+=15;
      return true;
    }
    return false;
  }
}
