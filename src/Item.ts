import {Canvas} from './Canvas.js';
import {Position} from './Position.js';
export class Item{
  private position:Position;
  private canvas:Canvas;
  public constructor(x:number, y:number,c:Canvas){
    this.position=new Position(x*20,y*20);
    this.canvas=c;
  }
  public draw():void{
    this.canvas.draw(this.position.getX(),this.position.getY(),20,20,'pink');
  }
  public getX():number{
    return this.position.getX();
  }
  public getY():number{
    return this.position.getY();
  }
}
